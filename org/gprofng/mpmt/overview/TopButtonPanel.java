/* Copyright (C) 2022-2025 Free Software Foundation

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses>.  */

package org.gprofng.mpmt.overview;

import org.gprofng.mpmt.util.gui.AnUtility;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionListener;
import javax.swing.JLabel;

public class TopButtonPanel extends javax.swing.JPanel {
  public TopButtonPanel(String labelText, String buttonText, ActionListener actionListener) {
    initComponents();
    setBackground(Color.WHITE);
    JLabel label = new JLabel(labelText);
    AnUtility.setAccessibleContext(label.getAccessibleContext(), label.getText());

    //        JTextArea textArea = new JTextArea();
    //        textArea.setText(labelText);
    //        textArea.setFont(textArea.getFont().deriveFont(Font.PLAIN));
    //        textArea.setLineWrap(true);
    //        textArea.setWrapStyleWord(true);
    //        textArea.setBorder(null);
    //        textArea.setOpaque(false);
    //        textArea.setMinimumSize(new Dimension(10, 10));
    //        textArea.setPreferredSize(new Dimension(10, 40));

    GridBagConstraints gridBagConstraints = new GridBagConstraints();
    gridBagConstraints.gridx = 0;
    gridBagConstraints.gridy = 0;
    gridBagConstraints.anchor = GridBagConstraints.SOUTHWEST;
    gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
    gridBagConstraints.weightx = 1.0;
    gridBagConstraints.insets = new Insets(0, 0, 3, 0);

    add(label, gridBagConstraints);

    button.setText(buttonText);
    button.addActionListener(actionListener);
    button.setFont(Overview.defaultPlainFont);
    button.setMargin(new Insets(0, 4, 0, 4));
    button.setBackground(new Color(237, 240, 244));
  }

  /**
   * This method is called from within the constructor to initialize the form. WARNING: Do NOT
   * modify this code. The content of this method is always regenerated by the Form Editor.
   */
  @SuppressWarnings("unchecked")
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {
    java.awt.GridBagConstraints gridBagConstraints;

    button = new javax.swing.JButton();

    setLayout(new java.awt.GridBagLayout());
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 1;
    gridBagConstraints.gridy = 0;
    gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHEAST;
    gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 8);
    add(button, gridBagConstraints);
  } // </editor-fold>//GEN-END:initComponents
  // Variables declaration - do not modify//GEN-BEGIN:variables
  private javax.swing.JButton button;
  // End of variables declaration//GEN-END:variables
}
